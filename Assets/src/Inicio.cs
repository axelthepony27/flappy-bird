using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inicio : MonoBehaviour
{
    private Bird bird;
    [SerializeField]
    SpawnerTuberías spawnerTuberías;
    [SerializeField]
    UI ui;
    
    void Start()
    {
        bird = GetComponent<Bird>();
    }

    public void IniciarJuego()
    {
        for(int i = 1; i < ui.transform.childCount; i++)
        {
            ui.transform.GetChild(i).gameObject.SetActive(false);
        }
        ui.transform.GetChild(0).gameObject.SetActive(true);
        spawnerTuberías.SetPuedeSpawnear(true);

        bird.SetJuegoIniciado(true);
        bird.SetGravityScale(1);
        bird.Salta();
    }
}
