using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tubería : MonoBehaviour 
{
    public float velocidad;

    private void Start() 
    {
        StartCoroutine(MueveIzquierda());
        Destroy(gameObject, 10);
    }

    public IEnumerator MueveIzquierda() 
    {
        while (true) 
        {
            transform.position += Vector3.left * velocidad * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.TryGetComponent(out Bird pajaro)) 
        {
            pajaro.MásUnPunto();
        }
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        Time.timeScale = 0;
    }
}
