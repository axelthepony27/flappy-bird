using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suelo : MonoBehaviour
{
    public float velocidad;

    private void Start()
    {
        StartCoroutine(MueveIzquierda());
        Destroy(gameObject, 12);
    }

    public IEnumerator MueveIzquierda()
    {
        while (true)
        {
            transform.position += Vector3.left * velocidad * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Time.timeScale = 0;
    }
}
