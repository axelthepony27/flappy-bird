using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerTuberías : MonoBehaviour
{

    [SerializeField]
    GameObject PrefabTuberías;
    [SerializeField]
    bool puedeSpawnear;

    private GameObject lastSpawn;

    // Start is called before the first frame update
    void Start()
    {
        puedeSpawnear = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (puedeSpawnear)
        {
            float tiempo = Random.Range(1.5f, 4);
            Spawn(tiempo);
        }
    }

    private void Spawn(float tiempoEntreSpawns)
    {
        StartCoroutine(BloquearSpawner(tiempoEntreSpawns));
        float rndYValue = Random.Range(-1.0f, 1.0f);
        lastSpawn = Instantiate(PrefabTuberías, transform.position + new Vector3(0.0f, rndYValue, 0.0f), Quaternion.identity);
    }

    public void SetPuedeSpawnear(bool newPuedeSpawnear)
    {
        puedeSpawnear = newPuedeSpawnear;
    }

    private IEnumerator BloquearSpawner(float tiempo)
    {
        // Completar: Bloquear el disparo del arma por el tiempo dado
        puedeSpawnear = false;
        yield return new WaitForSeconds(tiempo);
        puedeSpawnear = true;
    }
}
