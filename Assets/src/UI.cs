using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI : MonoBehaviour
{
    private GameObject marcador;
    private Text puntuación;
    [SerializeField]
    Bird bird;

    void Awake()
    {
        marcador = transform.GetChild(0).gameObject;
        puntuación = marcador.GetComponent<Text>();
        marcador.SetActive(false);
    }

    void Update()
    {
        puntuación.text = bird.GetPuntaje().ToString();
        if (Time.timeScale == 0)
        {
            marcador.SetActive(false);
        }
    }
}
