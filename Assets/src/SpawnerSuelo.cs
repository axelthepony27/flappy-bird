using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerSuelo : MonoBehaviour
{
    [SerializeField]
    GameObject PrefabSuelo;

    private GameObject lastSpawn, newSpawn;

    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {
        if (lastSpawn.transform.position.x <= 9.63)
        {
            Spawn();
        }
    }

    private void Spawn()
    {
        newSpawn = Instantiate(PrefabSuelo, transform.position, Quaternion.identity);
        lastSpawn = newSpawn;
    }
}
