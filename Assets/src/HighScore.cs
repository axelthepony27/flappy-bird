using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{
    private int puntaje;
    private Text puntuación;
    [SerializeField]
    int highScore = 0;
    string highScoreKey = "HighScore";
    [SerializeField]
    Bird bird;

    void Start()
    {
        highScore = PlayerPrefs.GetInt(highScoreKey, 0);
        puntuación = GetComponent<Text>();
    }

    void Update()
    {
        puntaje = bird.GetPuntaje();
        puntuación.text = highScore.ToString();
        if (Time.timeScale == 0)
        {
            if (puntaje > highScore)
            {
                PlayerPrefs.SetInt(highScoreKey, puntaje);
                highScore = puntaje;
                PlayerPrefs.Save();
            }
        }
    }
}
