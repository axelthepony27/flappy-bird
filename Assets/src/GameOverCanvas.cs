using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverCanvas : MonoBehaviour
{
    private GameObject gameOverPanel;
    // Start is called before the first frame update
    void Start()
    {
        gameOverPanel = transform.GetChild(0).gameObject;
        gameOverPanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale == 0)
        {
            gameOverPanel.SetActive(true);
        }
    }
}
