using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MarcadorFinal : MonoBehaviour
{
    private Text puntuación;
    [SerializeField]
    Bird bird;

    void Awake()
    {
        puntuación = GetComponent<Text>();
    }

    void Update()
    {
        puntuación.text = bird.GetPuntaje().ToString();
    }
}
