using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    public Rigidbody2D rb;
    public float velocidadImpulso;
    [SerializeField]
    bool juegoIniciado;

    [SerializeField]
    int puntaje;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        juegoIniciado = false;
    }

    private void Update() {
        if ((Input.GetButtonDown("Fire1") || Input.GetKeyDown("space")) && juegoIniciado) {
            Salta();
        }
    }

    public int GetPuntaje()
    {
        return puntaje;
    }

    public void SetPuntaje(int nuevoPuntaje)
    {
        puntaje = nuevoPuntaje;
    }

    public int MásUnPunto()
    {
        puntaje++;
        return puntaje;
    }

    public void SetGravityScale(int newGravityScale)
    {
        rb.gravityScale = newGravityScale;
    }

    public void SetJuegoIniciado(bool newJuegoIniciado)
    {
        juegoIniciado = newJuegoIniciado;
    }

    public void Salta()
    {
        rb.velocity = Vector2.up * velocidadImpulso;
    }
}